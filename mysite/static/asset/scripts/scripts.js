function Follow(object)
{
	$(object).find("span").text("Загрузка...");
	
	$.post( url + "post/Follow/" + uploader_user_id, { security: security })
    .done(function( data ) {
		if(data == "[OK]")
		{
			$(object).attr("onclick","UnFollow(this)");
	        $(object).addClass('following');
	        $(object).find("span").text("Отписаться");
		}else{
			$(object).find("span").text("Ошибка");
		}
    });
}

function UnFollow(object)
{
	$(object).find("span").text("Загрузка...");
	
	$.post( url + "post/UnFollow/" + uploader_user_id, { security: security })
    .done(function( data ) {
		if(data == "[OK]")
		{
			$(object).attr("onclick","Follow(this)");
	        $(object).removeClass('following');
	        $(object).find("span").text("Подписаться");
		}else{
			$(object).find("span").text("Ошибка");
		}
    });
}

function Search(){
	var method = $('#search_method').text();
	
	if(method == " VIDEOS")
	{
		method = 1;
	}
	else
	{
		method = 2;
	}
	
	window.location.href = url + "home/Search/" + $("#search").val() + "/" + method + "/";
	return false;
}

var comment_i = 10;
var videos_i = 10;

function escapeHtml(text) {
  var map = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#039;'
  };
  
   return text.replace(/[&<>"']/g, function(m) { return map[m]; });
}

function LikeOrUnlike(object){
	var status = $('#like_text').text();
	
	switch(status){
		case "Отмена": // Unlike
		    $(object).find("#like_text").text("Загрузка...");
		
			$.post( url + "post/unlike/" + hash, { security: security })
            .done(function( data ) {
               if(data == "[UNLIKED]"){
	        	   $(object).parent().find('#likes').text(parseInt($(object).parent().find('#likes').text()) - 1);
	        	   $(object).find('#like_text').text('Нравиться');
	           }else{
				   $(object).find("#like_text").text("Ошибка");
			   }
            });
		break;
		
		case "Нравиться": // Like
		    $(object).find("#like_text").text("Загрузка...");
			
			$.post( url + "post/like/" + hash, { security: security })
            .done(function( data ) {
               if(data == "[LIKED]"){
	        	   $(object).parent().find('#likes').text(parseInt($(object).parent().find('#likes').text()) + 1);
	        	   $(object).find('#like_text').text('Отмена');
	           }else{
				   $(object).find("#like_text").text("Ошибка");
			   }
            });
		break;
	}
}

function COMMENT(object)
{
	var _comment = $("#comment_text").val();
    $("#comment_text").val("");
	$.post( url + "post/comment/" + hash, { comment: _comment, security: security })
    .done(function( data ) {
       if(data == "[OK]")
	   {
		   $('.comment-box').prepend('<div class="social-talk"> <div class="media social-profile clearfix"><a class="pull-left" href="' + url + 'home/Profile/' + login_username + '"><img src="' + url + 'profiles/avatar/' + avatar + '" alt="' + login_username + '"> </a><div class="media-body"><span class="font-bold"><a href="' + url + 'home/Profile/' + login_username + '">' + login_username + '</a></span> <small class="text-muted">JUST NOW</small><div class="social-content">' + escapeHtml(_comment) + '</div> </div></div></div><hr>');
	   }
	   else if(data == "[TIME]")
	   {
		   swal("Error!", "Please wait 60 seconds from your last comment!", "error");
	   }
	   else
	   {
		   swal("Error!", "Error while posting comment!", "error");
	   }
    });
}

function LoadComments(object)
{
	var def = $(object).text();
	
	$(object).text("LOADING...");
	
    $.post( url + "post/LoadComments/" + comment_i, { hash: hash })
    .done(function( data ) {
		 if(data == "[NULL]")
		 {
			 $(object).parent().html("<center>NO MORE COMMENTS</center>");
		 }
		 else
		 {
			comment_i += 10;
		 
		    $(object).text(def);
			
			$('.comment-box').append(data);
		 }
    });
}

function DeleteComment(id, object)
{
    swal({
                title: "Are you sure?",
                text: "Your will not be able to recover this comment!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false },
            function (isConfirm) {
                if (isConfirm) {
					$.post( url + "post/DeleteComment/" + id, { 'security': security })
                    .done(function( data ) 
	                {
						if(data == "[OK]")
						{
							swal("Deleted!", "The comment has been deleted.", "success");
							$(object).parent().parent().parent().remove();
						}
						else
						{
							swal("Error!", "Error while deleting comment!", "error");
						}
					});
                } 
				else 
				{
                    swal("Cancelled", "The comment is safe :)", "error");
                }
            });
}

function LoadVideos(object)
{
	var def = $(object).text();
	
	$(object).text("LOADING...");
	
    $.post( url + "post/LoadVideos/" + videos_i, { category: _category, hash: hash })
    .done(function( data ) {
         if(data == "[NULL]")
		 {
			 $(object).replaceWith("<center>NO MORE VIDEOS</center>");
		 }
		 else
		 {
			videos_i += 10;
		 
		    $(object).text(def);
			
			$('.video-box').append(data);
		 } 
    });
}

var checked_all = 0;

function GetChecked()
{
	var data = [];
	
    $("#select_msg:checked").each(function() {
      data.push($(this).val());
    });
	
	return data;
}

function DeleteMessages()
{
	var data = GetChecked();
	
	if (data.length !== 0) 
	{
	    swal({
            title: "Are you sure?",
            text: "Your will not be able to recover the message/s!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it/them!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: false },
        function (isConfirm) 
	    {
            if (isConfirm) 
	    	{
	    		$.post( url + "post/PM_Operation/1", { 'List': data, 'security': security })
                .done(function( data ) 
	    		{
                   if(data == "[OK]")
	               {
	    			   swal("Deleted!", "Selected message/s has been deleted.", "success");
	    			   
	            	   $("#select_msg:checked").each(function() {
                         $(this).parent().parent().parent().parent().remove();
                       });
	               }
	    		   else
	    		   {
	    			   swal("Cancelled", "Sorry, try again later.", "error");
	    		   }
                });
	    
            }
	    	else
	    	{
                swal("Cancelled", "Your message/s is/are safe :)", "error");
            }
        });			
	}
}

function MarkAll()
{
	if(checked_all == 0)
	{
		$("#select_msg:not(:checked)").each(function() {
           $(this).prop('checked', true);
        });
		
		checked_all = 1;
	}
	else
	{
		$("#select_msg:checked").each(function() {
           $(this).prop('checked', false);
        });
		
		checked_all = 0;
	}
}

function SeenMessages()
{
	var data = GetChecked();
	
	if (data.length !== 0) 
	{
		$.post( url + "post/PM_Operation/2", { 'List': data, 'security': security })
        .done(function( data ) 
	    {
			if(data == "[OK]")
			{
				 $("#select_msg:checked").each(function() {
                   $(this).parent().parent().parent().removeClass("unread");
				   $(this).prop('checked', false);
                 });
				 
				 checked_all = 0;
			}
		});
	}
}

function DeleteVideo(id, object)
{
    swal({
                title: "Are you sure?",
                text: "Your will not be able to recover this video!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false },
            function (isConfirm) {
                if (isConfirm) {
					$.post( url + "post/DeleteMyVideo/" + id, { 'security': security })
                    .done(function( data ) 
	                {
						if(data == "[OK]")
						{
							swal("Deleted!", "Your video has been deleted.", "success");
							
							$(object).parent().parent().parent().parent().remove();
						}
						else
						{
							swal("Error!", "Error while deleting video!", "error");
						}
					});
                } 
				else 
				{
                    swal("Cancelled", "Your video is safe :)", "error");
                }
            });
}

function EditVideo(id, title, desc, enable_comments)
{
	$('[name="edit_video_id"]').val(id);
	$('[name="edit_title"]').val(title);
	$('[name="edit_description"]').val(desc);
	$('[name="enable_comments"]').val(enable_comments);
}

$(".dropdown-menu li a").click(function(){
  $(this).parents(".btn-group").find('.selection').html($(this).html());
  
  $("#search").attr("placeholder", "Поиск" + $(this).text().toLowerCase() + "...")
});