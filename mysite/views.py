from django.contrib.auth import logout
from django.contrib.auth.models import User
from django.shortcuts import redirect, render
from django.views.generic import TemplateView


def logout_view(request):
    logout(request)
    return redirect("/")


class HomeView(TemplateView):
    template_name = "home.html"


class ContactsView(TemplateView):
    template_name = "contacts.html"


class ProfilePage(TemplateView):
    template_name = "registration/profile.html"


class RegisterView(TemplateView):
    template_name = "registration/register.html"

    def dispatch(self, request, *args, **kwargs):
        if 'username' in request.GET:
            username = request.GET['username']
            email = request.GET['email']
            password = request.GET['password']
            password2 = request.GET['password2']

            if password==password2:
                User.objects.create_user(username,email,password)

        return render(request, self.template_name)

